libclass-dbi-plugin-perl (0.03-7) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 11 Dec 2019 15:56:17 +0000

libclass-dbi-plugin-perl (0.03-6.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 16:28:43 +0100

libclass-dbi-plugin-perl (0.03-6) unstable; urgency=medium

  * Team upload

  [ Ansgar Burchardt ]
  * Update my email address.
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Mark package as source format 3.0 (quilt)
  * Update license paragraphs to commonly used versions
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.4
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Mon, 25 Jun 2018 23:26:30 +0200

libclass-dbi-plugin-perl (0.03-5) unstable; urgency=low

  [ Ryan Niebur ]
  * moved with permission from Bart (Closes: #531502)
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza);
    ${misc:Depends} to Depends: field. Removed: Homepage pseudo-field
    (Description). Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Bart Martens
    <bartm@knars.be>); Bart Martens <bartm@knars.be> moved to Uploaders.
  * debian/watch: use dist-based URL.
  * remove Bart from Uploaders

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Bump Standards-Version to 3.8.2.
  * Use debhelper 7 instead of cdbs.
  * Convert debian/copyright to proposed machine-readable format.
  * Add myself to Uploaders.
  * Mention module name in description.
  * Do no longer install README (copy of POD documentation).

  [ gregor herrmann ]
  * debian/control: lowercase short description.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 20 Jul 2009 15:55:26 +0200

libclass-dbi-plugin-perl (0.03-4) unstable; urgency=low

  * New maintainer, as agreed with Stephen.
  * debian/*: Use cdbs.
  * debian/watch: Updated to version 3.

 -- Bart Martens <bartm@knars.be>  Sun, 19 Nov 2006 15:00:32 +0100

libclass-dbi-plugin-perl (0.03-3) unstable; urgency=low

  * debian/control:
      - Moved debhelper to Build-Depends
      - Simplified Build-Depends-Indep
  * Switched to my debian.org email address

 -- Stephen Quinney <sjq@debian.org>  Tue, 20 Jun 2006 21:07:18 +0100

libclass-dbi-plugin-perl (0.03-2) unstable; urgency=high

  * Updated build dependency from libdbd-sqlite-perl to
    libdbd-sqlite3-perl, closes: #306127
  * Urgency is high as libdbd-sqlite-perl will be removed soon.

 -- Stephen Quinney <sjq@debian.org>  Mon, 25 Apr 2005 10:25:22 +0100

libclass-dbi-plugin-perl (0.03-1) unstable; urgency=low

  * Initial Release, closes: #287002

 -- Stephen Quinney <sjq@debian.org>  Sat, 15 Jan 2005 13:27:58 +0000
